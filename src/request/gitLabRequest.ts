import * as request from "request-promise-native";
import { Options } from "../options";

async function run<T = any>(options: Options, method: string, url: string, body: any): Promise<T> {
  const opts = {
    method,
    headers: {
      "Private-Token": options.gitlabToken,
      "Content-Type": "application/json"
    },
    url,
    json: true,
    proxy: options.proxy,
    body
  };

  const result: T = await request(opts);
  return result;
}

function get<T = any>(options: Options, url: string): Promise<T> {
  return run(options, "GET", `${options.gitlabUrl}/api/v4/${url}`, undefined);
}


function post<T = any>(options: Options, url: string, body: any): Promise<T> {
  return run(options, "POST", `${options.gitlabUrl}/api/v4/${url}`, body);
}

function graphQl<T = any>(options: Options, request: string): Promise<T> {
  return run(options, "POST", `${options.gitlabUrl}/api/graphql`, request);
}



async function getFile(options: Options, url: string): Promise<string> {
  const opts = {
    uri: `${options.gitlabUrl}/api/v4/${url}`,
    method: "GET",
    encoding: "binary",
    headers: {
      "Private-Token": options.gitlabToken,
      "Content-type": "application/text"
    }
  };
  const body = await request(opts);
  return body;
}


export const gitlabRequest = {
  post,
  get,
  getFile,
  graphQl
};
