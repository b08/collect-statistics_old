export enum Category {
  all = "all",
  support = "support",
  dry = "dry",
  feature = "feature",
  generator = "generator"
}
