import { Category } from "./libraryCategory.enum";
import { NDate } from "@b08/ndate";

export interface LibraryStatistic {
  library: Library;
  version: Version;
  metrics: Metrics;
}

export interface Library {
  name: string;
  category: Category;
  gitRepo: string;
  npmUrl: string;
}

export interface Version {
  major: number;
  minor: number;
  patch: number;
  publishedAt: NDate;
}

export interface Metrics {
  loc: number;
  tests: number;
  coverage: number; // percentage
}
