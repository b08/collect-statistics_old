import { LibraryStatistic } from "./type";
import { Options } from "./options";
import { getGitlabProjects } from "./getGitLabProjects";
import { getProjectStatistics } from "./getProjectStatistics";

export async function collectStatistics(options: Options, start: number = 0): Promise<LibraryStatistic[]> {
  const projects = await getGitlabProjects(start, options);
  if (projects.length === 0) { return []; }
  const [thisStats, restOfStats] = await Promise.all([
    Promise.all(projects.map(p => getProjectStatistics(p, options))),
    collectStatistics(options, projects[projects.length - 1])
  ]);
  const result = [...thisStats, ...restOfStats].filter(x => x != null);
  return result;
}

