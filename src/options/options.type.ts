export interface Options {
  gitlabUrl: string;
  gitlabToken: string;
  proxy: string;
}
