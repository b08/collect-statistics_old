import { LibraryStatistic } from "./type";
import { Options } from "./options";
import { gitlabRequest } from "./request/gitLabRequest";
import { max } from "@b08/array";
import { parsePipelineJobOutput } from "./parsePipelineJobOutput";

export async function getProjectStatistics(projectId: number, options: Options): Promise<LibraryStatistic> {
  const jobs: any[] = await getJobs(projectId, options);
  const filtered = jobs.filter(j => j.name === "release");
  const job = max(filtered, j => j.id);
  if (job == null) { return null; }
  const file = await getJobFile(projectId, job.id, options);
  const statistic = parsePipelineJobOutput(file);
  return statistic;
}

function getJobs(projectId: number, options: Options): Promise<any[]> {
  return gitlabRequest.get(options, `projects/${projectId}/jobs`);
}

function getJobFile(projectId: number, jobId: number, options: Options): Promise<string> {
  return gitlabRequest.getFile(options, `projects/${projectId}/jobs/${jobId}/trace`);
}
