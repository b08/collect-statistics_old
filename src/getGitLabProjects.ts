import { Options } from "./options";
import { gitlabRequest } from "./request/gitLabRequest";

export async function getGitlabProjects(start: number, options: Options): Promise<number[]> {
  const result = await gitlabRequest.get(options, `projects?owned=true&statistics=false&id_after=${start}&order_by=id&sort=asc`);
  return result.map(res => res.id);
}
