import * as fse from "fs-extra";
import { collectStatistics } from "../src/collectStatistics";

async function collect(): Promise<void> {
  try {
    const file = await fse.readFile("../options.json", "utf8");
    const options = JSON.parse(file);
    const stats = await collectStatistics(options);
    console.log(stats);
  } catch (err) {
    return;
  }
}

collect()
  .then(() => console.log("done"))
  .catch(err => {
    console.error(err);
  });
